/** @type {import("tailwindcss").Config} */
module.exports = {
  content: ["./src/**/*.{astro,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        serif: ["EB Garamond Variable"],
        sans: ["Inter Variable"]
      }
    }
  }
}