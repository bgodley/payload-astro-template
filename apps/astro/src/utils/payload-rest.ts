import qs from "qs";
interface FindResult<T> {
  docs: Array<T>;
  totalDocs: number;
  limit: number;
  totalPages: number;
  page: number;
  pagingCounter: number;
  hasPrevPage: boolean;
  hasNextPage: boolean;
  prevPage: unknown;
  nextPage: unknown;
}

type Conditional = {
  [key: string]:
    | {
        equals: string | number | boolean;
      }
    | {
        not_equals: string | number | boolean;
      }
    | {
        greater_than: string | number;
      }
    | {
        greater_than_equal: string | number;
      }
    | {
        less_than: string | number;
      }
    | {
        less_than_equal: string | number;
      }
    | {
        like: string;
      }
    | {
        contains: string;
      }
    | {
        in: Array<string> | Array<number>;
      }
    | {
        not_in: Array<string> | Array<number>;
      }
    | {
        all: Array<string> | Array<number>;
      }
    | {
        exists: boolean;
      }
    | {
        near: `${number},${number},${number | null},${number | null}`;
      };
};

type Where =
  | Conditional
  | {
      or: Conditional[];
    }
  | {
      and: Conditional[];
    };

interface FindOptions {
  where?: Where;
  sort?: `${"-" | "+"}${string}`;
  limit?: number;
  page?: number;
}

export async function find<T>(
  slug: string,
  options?: FindOptions
): Promise<FindResult<T> | null> {
  try {
    const queryString = qs.stringify(options);

    const url = `${process.env.PAYLOAD_URL}/api/${slug}?${queryString}`;

    const res = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (res.ok) {
      return (await res.json()) as FindResult<T>;
    } else {
      return null;
    }
  } catch (err) {
    console.error(err);
    return null;
  }
}
