import type { Navigation } from "@turbopress/payload/types";

const getNavigationData = async (): Promise<Navigation> => {
  const res = await fetch(`${process.env.PAYLOAD_URL}/api/globals/navigation?en=true`);

  const navigation = await res.json() as Navigation;

  return navigation;
};

export default getNavigationData;