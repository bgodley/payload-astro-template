import type { Post } from "@turbopress/payload/types";

const getPostData = async (slug: string): Promise<Post | null> => {
  try {
    const res = await fetch(`${process.env.PAYLOAD_URL}/api/post?where[slug][equals]=${slug}&depth=10`);

    const data = await res.json();

    if (res.ok && data.docs && data.docs[0] !== undefined) {
      return data.docs[0] as Post;
    } else {
      return null;
    }
  } catch (err) {
    console.error(err);
    return null;
  }
}

export default getPostData;