import { buildConfig } from "payload/config";
import path from "path";
import Users from "./collections/Users";
import Media from "./collections/Media";
import Posts from "./collections/Post";
import ReusableContent from "./collections/ReusableContent";
import Navigation from "./globals/Navigation";
import Page from "./collections/Page";

export default buildConfig({
  serverURL: process.env.PAYLOAD_URL,
  admin: {
    user: Users.slug,
  },
  collections: [Page, Posts, Media, ReusableContent, Users],
  globals: [Navigation],
  typescript: {
    outputFile: path.resolve(__dirname, "..", "types", "payload.ts"),
  },
  cors: "*",
});
