import type { Block } from 'payload/types'

export const ReusableContentBlock: Block = {
  slug: 'reusable-content-block',
  fields: [
    {
      name: "reusableContentBlockFields",
      label: false,
      type: 'group',
      admin: {
        hideGutter: true,
        style: {
          margin: 0,
          padding: 0,
        },
      },
      fields: [
        {
          name: 'reusableContent',
          type: 'relationship',
          relationTo: 'reusable-content',
          required: true,
        },
      ],
    },
  ],
}