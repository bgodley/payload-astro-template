import { Block } from "payload/types";

const PictureBlock: Block = {
  slug: "picture",
  labels: {
    singular: "Picture",
    plural: "Pictures",
  },
  fields: [
    {
      name: "picture",
      label: "Picture",
      type: "upload",
      relationTo: "media",
      required: true,
    },
    {
      name: "alt",
      label: "Alt Text",
      type: "text",
    },
    {
      name: "caption",
      label: "Caption",
      type: "textarea",
    },
  ],
};

export default PictureBlock;