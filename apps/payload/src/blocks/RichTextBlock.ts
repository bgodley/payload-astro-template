import { Block } from "payload/types";

const RichTextBlock: Block = {
  slug: "rich-text",
  labels: {
    singular: "Rich Text",
    plural: "Rich Text",
  },
  fields: [
    {
      name: "content",
      label: "Content",
      type: "richText",
      required: true,
    },
  ],
};

export default RichTextBlock;