import PictureBlock from "./PictureBlock";
import { ReusableContentBlock } from "./ReusableContentBlock";
import RichTextBlock from "./RichTextBlock";
import SectionBlock from "./SectionBlock";

export const allBlocks = [RichTextBlock, PictureBlock, SectionBlock, ReusableContentBlock];