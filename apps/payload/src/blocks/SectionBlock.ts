import { Block } from "payload/types";
import PictureBlock from "./PictureBlock";
import RichTextBlock from "./RichTextBlock";
import { ReusableContentBlock } from "./ReusableContentBlock";

const SectionBlock: Block = {
  slug: "section",
  labels: {
    singular: "Section Block",
    plural: "Section Blocks"
  },
  fields: [
    {
      name: "title",
      label: "Title",
      type: "text",
      required: false,
      admin: {
        position: "sidebar"
      }
    },
    {
      name: "description",
      label: "Description",
      type: "text",
      required: false,
      admin: {
        position: "sidebar"
      }
    },
    {
      name: "blocks",
      label: "Blocks",
      type: "blocks",
      blocks: [PictureBlock, RichTextBlock, ReusableContentBlock],
      required: true,
    }
  ]
}

export default SectionBlock;