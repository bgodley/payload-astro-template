import { Block } from "payload/types";
import NavLinkBlock from "./NavLinkBlock";

const NavDropdownBlock: Block = {
  slug: "nav-dropdown",
  fields: [
    {
      name: "text",
      type: "text",
      required: true,
    },
    {
      name: "links",
      label: "Dropdown Links",
      type: "blocks",
      blocks: [NavLinkBlock],
      required: true,
    }
  ]
};

export default NavDropdownBlock;