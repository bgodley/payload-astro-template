import { Block } from "payload/types";
import Post from "../collections/Post";
import Page from "../collections/Page";

const NavLinkBlock: Block = {
  slug: "nav-link",
  fields: [
    {
      name: "text",
      type: "text",
      required: true,
    },
    {
      type: "row",
      fields: [
        {
          name: "reference",
          type: "radio",
          options: [
            {
              label: "Page",
              value: "page",
            },
            {
              label: "Post",
              value: "post",
            },
            {
              label: "Custom URL",
              value: "url",
            },
          ],
        },
        {
          name: "post",
          type: "relationship",
          relationTo: Post.slug,
          admin: {
            condition: (_, siblingData) => {
              return siblingData.reference === "post";
            },
          },
        },
        {
          name: "page",
          type: "relationship",
          relationTo: Page.slug,
          admin: {
            condition: (_, siblingData) => {
              return siblingData.reference === "page";
            },
          },
        },
        {
          name: "customUrl",
          type: "text",
          admin: {
            condition: (_, siblingData) => {
              return siblingData.reference === "url";
            },
          },
        },
      ],
    },
  ],
};

export default NavLinkBlock;
