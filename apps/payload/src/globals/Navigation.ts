import { GlobalConfig } from "payload/types";
import NavLinkBlock from "../blocks/NavLinkBlock";
import NavDropdownBlock from "../blocks/NavDropdownBlock";

const Navigation: GlobalConfig = {
  slug: "navigation",
  access: {
    read: () => true,
  },
  fields: [
    {
      name: "siteName",
      type: "text",
      required: true,
    },
    {
      name: "blocks",
      type: "blocks",
      blocks: [NavLinkBlock, NavDropdownBlock],
      required: true,
    }
  ] 
};

export default Navigation;