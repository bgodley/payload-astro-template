import { CollectionConfig } from "payload/types";
import { allBlocks } from "../blocks";

const Post: CollectionConfig = {
  slug: "post",
  admin: {
    useAsTitle: "title",
  },
  access: {
    read: () => true,
  },
  fields: [
    {
      name: "title",
      label: "Title",
      type: "text",
      required: true,
    },
    {
      name: "slug",
      label: "Slug",
      type: "text",
      required: true,
      admin: {
        position: "sidebar",
      }
    },
    {
      name: "content",
      label: "Content",
      type: "blocks",
      blocks: allBlocks,
      required: true
    },
  ]
};

export default Post;