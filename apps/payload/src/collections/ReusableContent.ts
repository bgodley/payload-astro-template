import { CollectionConfig } from "payload/types";
import { allBlocks } from "../blocks";

const ReusableContent: CollectionConfig = {
  slug: "reusable-content",
  access: {
    read: () => true,
  },
  labels: {
    singular: "Reusable Content",
    plural: "Reusable Content",
  },
  admin: {
    useAsTitle: "title",
  },
  fields: [
    {
      name: "title",
      type: "text",
      required: true,
    },
    {
      name: "layout",
      type: "blocks",
      required: true,
      blocks: allBlocks
    },
  ],
};

export default ReusableContent;