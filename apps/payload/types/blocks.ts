import { ReusableContent } from "./payload";
export type Block = ReusableContent["layout"][0];
